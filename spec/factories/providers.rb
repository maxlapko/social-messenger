# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :provider do
    nickname "MyString"
    uid "MyString"
    provider "MyString"
    oauth_token "MyString"
    oauth_secret "MyString"
  end
end
