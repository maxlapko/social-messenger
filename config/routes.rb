SocialMessager::Application.routes.draw do
  authenticated :user do
    root :to => 'home#index'
  end

  root :to => "home#index"
  devise_for :users
  resources :users, :only => [:show, :index]
  match 'auth/:provider/callback', to: 'providers#create'
  resources :providers, :only => [:create, :index, :destroy]
  resources :messages
end
