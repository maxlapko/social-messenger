class Provider
  include Mongoid::Document
  field :nickname
  field :uid
  field :provider
  field :oauth_token
  field :oauth_secret

  belongs_to :user

  def gateway
    return @gateway if @gateway
    @gateway = Twitter::Client.new(:oauth_token => oauth_token, :oauth_token_secret => oauth_secret) if provider == 'twitter'
    @gateway = VK::Serverside.new :app_id => ENV['API_KEY'], :app_secret => ENV['API_SECRET'], :access_token => oauth_token if provider == 'vkontakte'
    GatewayAdapter.new @gateway, provider
  end
end
