class Message
  STATUSES = {
    :new         => 'new',
    :sent        => 'sent',
    :processing  => 'processing'
  }

  include Mongoid::Document
  include Mongoid::Timestamps

  field :content
  field :status, :default => STATUSES[:new]
  field :sent_at, :type => DateTime

  validates_presence_of :content, :provider_ids
  attr_accessible :content, :provider_ids

  embedded_in :user
  has_and_belongs_to_many :providers, inverse_of: nil

  def perform_send
    return true unless status == STATUSES[:new]
    providers.each do |p|
      p.gateway.post_message content
    end
    self.status = STATUSES[:sent]
    self.sent_at = Time.now
    save!
  end
end
