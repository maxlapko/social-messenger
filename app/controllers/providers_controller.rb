class ProvidersController < ApplicationController
  before_filter :authenticate_user!

  def index
    @providers = current_user.providers
  end

  def create
    @provider = current_user.provider_from_omniauth(env["omniauth.auth"])
    redirect_to providers_path, :notice => "Successfully connected to #{@provider.provider}"
  end

  def destroy
    @provider = current_user.providers.find params['id']
    @provider.destroy
    redirect_to providers_path, :notice => "Provider successfully deleted."
  end

end
