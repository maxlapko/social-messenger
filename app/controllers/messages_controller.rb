class MessagesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_providers, :only => [:edit, :create, :new, :update]

  def index
    @messages = current_user.messages
  end

  def new
    @message = Message.new
  end

  def create
    @message = current_user.messages.build params[:message]
    if @message.save
      redirect_to messages_path, notice: 'Message was successfully created.'
    else
      render "new"
    end
  end

  def edit
    @message = current_user.messages.find params[:id]
  end

  def update
    @message = current_user.messages.find params[:id]
    if @message.update_attributes params[:message]
      redirect_to messages_path, notice: 'Message was successfully updated.'
    else
      render "edit"
    end
  end

  def destroy
    @message = current_user.messages.find params[:id]
    @message.destroy
    redirect_to messages_path, notice: 'Message was successfully deleted.'
  end

  private

  def check_providers
    if current_user.providers.count == 0
      redirect_to providers_path, :alert => 'You should connect providers, before creating messages'
    end
  end

end
