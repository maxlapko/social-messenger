class GatewayAdapter

  def initialize(gateway, provider)
    @gateway, @provider = gateway, provider
  end

  def post_message(content, params = {})
    return false unless @gateway
    return @gateway.update(content) if @provider == 'twitter'
    return @gateway.wall.post({:message => content}) if @provider == 'vkontakte'
  end

end